# FLiPPer

FunctionaL Pathway and Pertubation analysis tookit (FLiPPer) is a pipeline which combines RNA-seq and SNV (single nucleotide variation) genotype data to assist in pathway discovery, functional enrichment, and testing sources of genetic pertubation.
FLiPPer utilizes WGCNA's supervised hierachical clustering methods to identify gene clusters or modules that are coexpressed and thus functionally related. Next, using clusterprofiler, we enrich pathways with functional data from many sources, including GO, Reactome, BioCarta, WikiPathways, and Nature. 
Finally, we utilize MatrixEQTL to test the affects of genetic pertubation through SNVs on our networks. 

